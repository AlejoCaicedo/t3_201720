package model.data_structures;

public class Stack<E> implements IStack<E> {
	
	private nodeStack<E> top;
	
	public Stack()
	{
		top = null;
	}
	
	public Stack(E elem)
	{
		top = null;
		push(elem);
	}
	
	public void push(E item) {
		
		nodeStack<E> nuevo = new nodeStack<E>(item);
		nuevo.agregarSiguiente(top);
		top = nuevo;
	}

	public E pop() throws Exception {
		
		E retorno = top.darElem();
		
		nodeStack sig = top.darSiguiente();
		
		if(sig != null)
		{
			top = sig;
			return retorno;
		}else
		{
			throw new Exception("The Stack has no elements");
		}
		
	}
			
	
			//Con esta clase organzo la informacion
			private class nodeStack<T>
			{
				private nodeStack<T> siguiente;
				private T item;
				
				public nodeStack(T elem)
				{
					siguiente = null;
					item = elem;
				}
				
				public void agregarSiguiente(nodeStack<T> temp)
				{
					siguiente = temp;
				}
				
				public T darElem()
				{
					return item;
				}
				
				public nodeStack darSiguiente()
				{
					return siguiente;
				}

			}
	
}
