package model.data_structures;


public class Queue<E> implements IQueue<E>{

	
	private nodoQueue<E> first;
	private nodoQueue<E> last;
	
	
	public Queue()
	{
		first = null;
		last = null;
	}
	
	public Queue(E elem)
	{
		nodoQueue temp = new nodoQueue(elem);
		first = temp;
		last = temp;
	}
	
	
	public E dequeue() {
		
		E retorno = last.darElem();
		last = last.darSiguiente();
		return retorno;
	}


	public void enqueue(E item) {
		nodoQueue<E> agregar = new nodoQueue<E>(item);
		first.agregarSiguiente(agregar);
		first = agregar;
		
	}

		private class nodoQueue<T>
		{
			private nodoQueue<T> siguiente;
			private T item;
			
			public nodoQueue(T elem)
			{
				siguiente = null;
				item = elem;
			}
			
			public void agregarSiguiente(nodoQueue<T> temp)
			{
				siguiente = temp;
			}
			
			public T darElem()
			{
				return item;
			}
			
			public nodoQueue darSiguiente()
			{
				return siguiente;
			}

		}
	
}
