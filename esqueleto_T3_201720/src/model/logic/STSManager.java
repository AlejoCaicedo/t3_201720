package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{

	private Queue<BusUpdateVO> jsonBusUpdates;
	private Queue<Stops> StopsList;
	
	//La complejidad de este metodo es n^3 ;(
	public void readBusUpdate(File rtFile) {
		try{
			FileReader reader = new FileReader(rtFile);
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);

			Iterator tempIterator = jsonArray.iterator();
			while(tempIterator.hasNext())
			{
				BusUpdateVO temp = new BusUpdateVO();
				JSONObject tempData = (JSONObject)tempIterator.next();
				
				String tripID = (String) tempData.get("RouteNo");
				temp.setTripId(tripID);
				String lat = (String) tempData.get("RouteNo");
				temp.setTripId(lat);
				String lon = (String) tempData.get("RouteNo");
				temp.setTripId(lon);
				String stopID = (String) tempData.get("RouteNo");
				temp.setTripId(tripID);
				System.out.println(tripID);
				jsonBusUpdates.enqueue(temp);
			}
			
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		
	}

	@Override
	public IStack<StopVO> listStops(Integer tripID) {
		Stack retorno = new Stack();
		BusUpdateVO temp = (BusUpdateVO)jsonBusUpdates.dequeue();
		while((temp) != null)
		{ 		Integer tempId = temp.getTripId();
			if((tempId).equals(tripID))
			{
				//Este metodod agrega al stack los Stops
				checkStops(retorno, temp);
			}
		}
		
		
		return null;
	}

	private void checkStops(Stack retorno, BusUpdateVO temp) {
		loadStops();
		Stops tempStop = StopsList.dequeue();
		while(tempStop != null)
		{
			double dist = getDistance(temp.getLatitude()),temp.getLongitude(),tempStop.getStop_lat(),tempStop.getStop_lon());
			if(dist >= -70 && dist <= 70)
			{
				retorno.push(tempStop.getStopName());
			}
		}
			
		
	}
				//copie el este metodo de la guia
	public double getDistance(int lat1, int lon1, int lat2, int lon2) {
		        // TODO Auto-generated method stub
		        final int R = 6371*1000; // Radious of the earth
		    
		        Double latDistance = toRad(lat2-lat1);
		        Double lonDistance = toRad(lon2-lon1);
		        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		        Double distance = R * c;
		        Return distance;
		 
		    }

		  private Double toRad(Double value) {
		        return value * Math.PI / 180;
		    }


	
	
	@Override
	public void loadStops() {
		try{		
			BufferedReader in = new BufferedReader(new FileReader("data/stops.txt"));
			StopsList = new Queue<Stops>();
			String tempLine;
						tempLine = in.readLine();//Este es la linea que dice el formato
				while((tempLine = in.readLine()) != null)
				{
					String[] data = tempLine.split(",");
					if(data.length == 10)
					{
						
						Stops dataPoint = new Stops(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8], data[9]);
						StopsList.enqueue(dataPoint);
						
					}
				}
				in.close();
			}catch(IOException e)
			{
				e.printStackTrace();
			}
		
	}

}
