package model.logic;

public class ParadaBus {

	private String vehicleNumber;
	private int tripId;
	private String routeNumber;
	private String direction;
	private String destination;
	private int latitude;
	private int longitude;
	private String time;
	private String routeMap;
	
	
	
	public ParadaBus()
	{
		vehicleNumber = null;
		tripId = 0;
		routeNumber = null;
		direction = null;
		destination = null;
		latitude = 0;
		longitude = 0;
		time = null;
		routeMap = null;
	}



	public int getTripId() {
		return tripId;
	}



	public void setTripId(String tripId) {
		this.tripId = Integer.parseInt(tripId);
	}



	public int getLatitude() {
		return latitude;
	}



	public void setLatitude(String latitude) {
		this.latitude = Integer.parseInt(latitude);
	}



	public int getLongitude() {
		return longitude;
	}



	public void setLongitude(String longitude) {
		this.longitude = Integer.parseInt(longitude);
	}



	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}



	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}



	public void setDirection(String direction) {
		this.direction = direction;
	}



	public void setDestination(String destination) {
		this.destination = destination;
	}



	public void setTime(String time) {
		this.time = time;
	}



	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}
	
	
	
	
	
	
}
